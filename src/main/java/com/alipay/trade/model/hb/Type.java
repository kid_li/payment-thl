package com.alipay.trade.model.hb;

/**
 * Created by liuyangkly on 15/8/27.
 */
public enum Type {
    /**
     * Cr type.
     */
    CR       // 收银机

    ,
    /**
     * Store type.
     */
    STORE    // 门店

    ,
    /**
     * Vm type.
     */
    VM       // 售卖机

    ,
    /**
     * Md type.
     */
    MD       // 医疗设备

    ,
    /**
     * Soft pos type.
     */
    SOFT_POS // 软POS

    ,
    /**
     * Pos type.
     */
    POS      // POS终端

    ,
    /**
     * Ali pos type.
     */
    ALI_POS  // 支付宝POS
}
